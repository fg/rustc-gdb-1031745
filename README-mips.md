two more test cases only failing on mipsel with gdb 13 (working with gdb 12.1-4
from snapshots.debian.org)

prerequisites: cargo and rustc from bookworm

compilation:

```shell
$ RUSTC_BOOTSTRAP=1 cargo build --all
```

working output test case 1 (src/bin/box.rs):

```shell
$ gdb --batch --command=gdb-box ./target/debug/box
Breakpoint 1 at 0x9248: file src/bin/box.rs, line 9.
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/mipsel-linux-gnu/libthread_db.so.1".

Breakpoint 1, box::main () at src/bin/box.rs:9
9           zzz();
$1 = 1
$2 = (2, 3.5)
```

working output test case 2 (src/bin/borrow-unique-basic.rs):

```shell
$ gdb --batch --command=gdb-borrowed-unique-basic ./target/debug/borrowed-unique-basic
Breakpoint 1 at 0x9d80: file src/bin/borrowed-unique-basic.rs, line 48.
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/mipsel-linux-gnu/libthread_db.so.1".

Breakpoint 1, borrowed_unique_basic::main () at src/bin/borrowed-unique-basic.rs:48
48          zzz();
$1 = true
$2 = -1
$3 = 97
$4 = 68
$5 = -16
$6 = -32
$7 = -64
$8 = 1
$9 = 100
$10 = 16
$11 = 32
$12 = 64
$13 = 2.5
$14 = 3.5
```

after upgrading gdb to 13.1-2

broken output test case 1 (src/bin/box.rs):

```shell
$ gdb --batch --command=gdb-box ./target/debug/box
Breakpoint 1 at 0x9310: file src/bin/box.rs, line 12.
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/mipsel-linux-gnu/libthread_db.so.1".

Breakpoint 1, box::zzz () at src/bin/box.rs:12
12      fn zzz() { () }
gdb-box:3: Error in sourced command file:
No symbol 'a' in current context
```

broken output test case 2 (src/bin/borrow-unique-basic.rs):

```shell
$ gdb --batch --command=gdb-borrowed-unique-basic ./target/debug/borrowed-unique-basic
Breakpoint 1 at 0x9f98: file src/bin/borrowed-unique-basic.rs, line 51.
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/mipsel-linux-gnu/libthread_db.so.1".

Breakpoint 1, borrowed_unique_basic::zzz () at src/bin/borrowed-unique-basic.rs:51
51      fn zzz() {()}
gdb-borrowed-unique-basic:3: Error in sourced command file:
No symbol 'bool_ref' in current context
```
