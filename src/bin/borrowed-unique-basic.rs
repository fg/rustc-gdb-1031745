#![allow(unused_variables)]
#![feature(omit_gdb_pretty_printer_section)]
#![omit_gdb_pretty_printer_section]

fn main() {
    let bool_box: Box<bool> = Box::new(true);
    let bool_ref: &bool = &*bool_box;

    let int_box: Box<isize> = Box::new(-1);
    let int_ref: &isize = &*int_box;

    let char_box: Box<char> = Box::new('a');
    let char_ref: &char = &*char_box;

    let i8_box: Box<i8> = Box::new(68);
    let i8_ref: &i8 = &*i8_box;

    let i16_box: Box<i16> = Box::new(-16);
    let i16_ref: &i16 = &*i16_box;

    let i32_box: Box<i32> = Box::new(-32);
    let i32_ref: &i32 = &*i32_box;

    let i64_box: Box<i64> = Box::new(-64);
    let i64_ref: &i64 = &*i64_box;

    let uint_box: Box<usize> = Box::new(1);
    let uint_ref: &usize = &*uint_box;

    let u8_box: Box<u8> = Box::new(100);
    let u8_ref: &u8 = &*u8_box;

    let u16_box: Box<u16> = Box::new(16);
    let u16_ref: &u16 = &*u16_box;

    let u32_box: Box<u32> = Box::new(32);
    let u32_ref: &u32 = &*u32_box;

    let u64_box: Box<u64> = Box::new(64);
    let u64_ref: &u64 = &*u64_box;

    let f32_box: Box<f32> = Box::new(2.5);
    let f32_ref: &f32 = &*f32_box;

    let f64_box: Box<f64> = Box::new(3.5);
    let f64_ref: &f64 = &*f64_box;

    zzz();
}

fn zzz() {()}
