#![allow(unused_variables)]
#![feature(omit_gdb_pretty_printer_section)]
#![omit_gdb_pretty_printer_section]

fn main() {
    let a = Box::new(1);
    let b = Box::new((2, 3.5f64));

    zzz();
}

fn zzz() { () }
