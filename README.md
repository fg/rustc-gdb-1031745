extracted reproducer for gdb breaking rustc gdb test case

`src/main.rs` (code) and `gdb` (gdb commands) copied/extracted from
`tests/debuginfo/unsized.rs`[0]

to reproduce, run

```shell
$ RUSTC_BOOTSTRAP=1 cargo build
$ gdb --batch --command=gdb ./target/debug/rustc-gdb-1031745
```

problematic (broken) output:

```
Breakpoint 1 at 0x81dd: file src/main.rs, line 88.
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".

Breakpoint 1, rustc_gdb_1031745::main () at src/main.rs:88
88	    zzz(); // #break
$1 = &rustc_gdb_1031745::Foo<[u8]> 0x7fffffffdd88
$2 = &rustc_gdb_1031745::Foo<rustc_gdb_1031745::Foo<[u8]>> 0x7fffffffdd88
$3 = &rustc_gdb_1031745::Foo<dyn core::fmt::Debug> {pointer: 0x555555593034, vtable: 0x5555555a3000warning: (Internal error: pc 0x5555555a3000 in read in CU, but not in symtab.)
warning: (Error: pc 0x5555555a3000 in address map, but not in symtab.)
}
$4 = alloc::boxed::Box<rustc_gdb_1031745::Foo<dyn core::fmt::Debug>, alloc::alloc::Global> {pointer: 0x5555555a7ba0, vtable: 0x5555555a3000warning: (Internal error: pc 0x5555555a3000 in read in CU, but not in symtab.)
warning: (Error: pc 0x5555555a3000 in address map, but not in symtab.)
}
$5 = &(i32, i32, [i32]) [(0, 1, 0), (2, 3, 0)]
$6 = &(i32, i32, dyn core::fmt::Debug) {pointer: 0x5555555a3020, vtable: 0x5555555a3030warning: (Internal error: pc 0x5555555a3030 in read in CU, but not in symtab.)
warning: (Error: pc 0x5555555a3030 in address map, but not in symtab.)
}
```

OK output after downgrading gdb to `12.1-4+b1` from bookworm:

```
Breakpoint 1 at 0x81dd: file src/main.rs, line 88.
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".

Breakpoint 1, rustc_gdb_1031745::main () at src/main.rs:88
88	    zzz(); // #break
$1 = &rustc_gdb_1031745::Foo<[u8]> {data_ptr: 0x7fffffffdd88, length: 4}
$2 = &rustc_gdb_1031745::Foo<rustc_gdb_1031745::Foo<[u8]>> {data_ptr: 0x7fffffffdd88, length: 4}
$3 = &rustc_gdb_1031745::Foo<dyn core::fmt::Debug> {pointer: 0x555555593034, vtable: 0x5555555a3000}
$4 = alloc::boxed::Box<rustc_gdb_1031745::Foo<dyn core::fmt::Debug>, alloc::alloc::Global> {pointer: 0x5555555a7ba0, vtable: 0x5555555a3000}
$5 = &(i32, i32, [i32]) {data_ptr: 0x555555593038, length: 2}
$6 = &(i32, i32, dyn core::fmt::Debug) {pointer: 0x5555555a3020, vtable: 0x5555555a3030}
```

0: https://github.com/rust-lang/rust/blob/f4c7596ac3f2d27578787da3279705fd45aefbd6/tests/debuginfo/unsized.rs
